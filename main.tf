resource "aws_instance" "ec2_hocine" {
  ami           = "ami-09e1162c87f73958b"
  instance_type = "t3.micro"
  associate_public_ip_address = true
  key_name               ="hocine_key"
  subnet_id              = "subnet-028f7b03006704ed7"

  tags = {
    Name = "test-jenkins-hs"
  }
}


